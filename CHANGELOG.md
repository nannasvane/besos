## Changelog

### [Development](https://gitlab.com/energyincities/besos/-/tree/dev)
#### Added
#### Changed
* [!127](https://gitlab.com/energyincities/besos/-/merge_requests/127) Bug fix for Sphinx deprecation version argument
#### Removed

### [Version 2.1.3](https://gitlab.com/energyincities/besos/-/releases#2.1.3)
#### Added
#### Changed
* [!127](https://gitlab.com/energyincities/besos/-/merge_requests/127) Bug fix for Sphinx deprecation version argument
#### Removed

### [Version 2.1.0](https://gitlab.com/energyincities/besos/-/releases#2.1.0)
#### Added
* [!121](https://gitlab.com/energyincities/besos/-/merge_requests/121) Added collect_rst.py to integrate besos-examples cleanly and automatically into readthedocs (described in docs/README.md)
* [!93](https://gitlab.com/energyincities/besos/-/merge_requests/93) Improved test coverage, adding multiple tests (listed in merge request)
* [!123](https://gitlab.com/energyincities/besos/-/merge_requests/123) Added functionality for rbf_opt to work with CategoryParameters along with RangeParameters
#### Changed
* [!122](https://gitlab.com/energyincities/besos/-/merge_requests/122) Corrected IDF_class.remove_shading() where invalid shading objects were edited
* [!118](https://gitlab.com/energyincities/besos/-/merge_requests/118) Improved warning messages
#### Removed
* [!93](https://gitlab.com/energyincities/besos/-/merge_requests/93) Removed unused functions (eppy_funcs.get_files(), eplus_funcs.check_idf_version(), pyehub_funcs.name_getter())
* [!116](https://gitlab.com/energyincities/besos/-/merge_requests/116) Removed config.yaml and put information in config.py as solver_settings

### [Version 2.0.0](https://gitlab.com/energyincities/besos/-/releases#2.0.0)
#### Added
* [!124](https://gitlab.com/energyincities/besos/-/merge_requests/124) Bug fix where EnergyPlus would overwrite current directory on failure.
* [!113](https://gitlab.com/energyincities/besos/-/merge_requests/113) Added besos_utils to contain helper functions that can be used throughout the codebase.
* [!111](https://gitlab.com/energyincities/besos/-/merge_requests/111) eplus_funcs now contains function `get_idf_version_pth()` which retrieve IDF's version without creating an IDF object for improved runtime.
* [!111](https://gitlab.com/energyincities/besos/-/merge_requests/111) `InstallationError` added to provide more descriptive error handling within get_ep_path
* [!60](https://gitlab.com/energyincities/besos/-/merge_requests/60) Evaluator caches now support data that contains timeseries, and many iterables.
* [!60](https://gitlab.com/energyincities/besos/-/merge_requests/60) Evaluators can be run in parallel by setting the `processes` argument to more than one.
*  [!54](https://gitlab.com/energyincities/besos/-/merge_requests/54)
 Parameters can now have more than one Descriptor, using the `value_descriptors`
 initialization argument. Functions used in GenericSelectors can
 take more than one input.
*  [!54](https://gitlab.com/energyincities/besos/-/merge_requests/54)
 Parameters can now have more than one Descriptor, using the `value_descriptors`
 initialization argument. Functions used in GenericSelectors can
 take more than one input.
#### Changed
* [!60](https://gitlab.com/energyincities/besos/-/merge_requests/60) Evaluators default to one process. The `processes` argument will no longer be silently ignored by some evaluators.
* [!60](https://gitlab.com/energyincities/besos/-/merge_requests/60) Optimizers always run single-threaded evaluators. This avoids them hanging due to a conflict between platypus and Evaluators.
* [!60](https://gitlab.com/energyincities/besos/-/merge_requests/60) Evaluators `keep_dirs` argument now works when using any number of processes.
* [!97](https://gitlab.com/energyincities/besos/-/merge_requests/97) The VariableReader now works with set frequencies
* [!97](https://gitlab.com/energyincities/besos/-/merge_requests/97) Added an example to the custom Evaluator example jupyter notebook to show how to use VariableReader. I've also renamed this to custom Objective jupyter notebook. This name reflects that the example notebook about objectives, how to set them and how to plug in custom functions.
* [!111](https://gitlab.com/energyincities/besos/-/merge_requests/111) Prior to running building file, the IDF version will be compared to the installed version of EnergyPlus and ensure matching versions or errors will be raised.
#### Deprecated
*  [54!](https://gitlab.com/energyincities/besos/-/merge_requests/54)
 deprecate `value_descriptor` as initialization argument for Parameter.
#### Removed
*  [!58](https://gitlab.com/energyincities/besos/-/merge_requests/58)
 The `eppySupport` module has been removed.
*  [54!](https://gitlab.com/energyincities/besos/-/merge_requests/54)
 `DependantParameter` and related methods have been removed.
* [!60](https://gitlab.com/energyincities/besos/-/merge_requests/60) Evaluators no longer accept the `multi` or `distributed` arguments. `processes` is used instead.
* [!111](https://gitlab.com/energyincities/besos/-/merge_requests/111) Removal of `config.energy_plus_version` as run_energyplus now detects IDF or epJSON version.

### [Version 1.7.1](https://gitlab.com/energyincities/besos/-/releases#1.7.1)
#### Changed
* [921fe55](https://gitlab.com/energyincities/besos-/commit/921fe55198428f144a3f1c8545285074f2f14237) JOSS paper edits
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) JOSS Paper edits, RTD fixes/updates
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) Simplified requirements (core/complete/dev)
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) Moved pytests to top level and are no longer distributed with pip installs
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) Updated example notebooks
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) `types.py` is no `besostypes.py` to avoid conflict with other "types"
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) Cleaned up imports
#### Removed
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) Moved all example files to [besos-examples](https://gitlab.com/energyincities/besos-examples) repo.  These are now only installed with `besos[complete]`
* [!112](https://gitlab.com/energyincities/besos/-/merge_requests/112) Removed extra config.yaml file

### [Version 1.7.0](https://gitlab.com/energyincities/besos/-/releases#1.7.0)
#### Removed
* [!90](https://gitlab.com/energyincities/besos/-/merge_requests/90) Removed `utils.py` and the contained functions `create_temp_directory` and `resolve_path`
#### Added
* [eb841aa](https://gitlab.com/energyincities/besos/-/commit/eb841aad0a65efa5b1fe04075c175311ee4c607e) Pre-commit hooks for all files
* [eb841aa](https://gitlab.com/energyincities/besos/-/commit/eb841aad0a65efa5b1fe04075c175311ee4c607e) Paper for JOSS submission
#### Changed
* [eb841aa](https://gitlab.com/energyincities/besos/-/commit/eb841aad0a65efa5b1fe04075c175311ee4c607e) Testing has been up dated to reflect missing any missing software and any expected fails
* [eb841aa](https://gitlab.com/energyincities/besos/-/commit/eb841aad0a65efa5b1fe04075c175311ee4c607e) Updated documentation throughout (both RTD and doc strings)
* [eb841aa](https://gitlab.com/energyincities/besos/-/commit/eb841aad0a65efa5b1fe04075c175311ee4c607e) Added coverage for pytests

### [Version 1.6.1](https://gitlab.com/energyincities/besos/-/releases#1.6.1)
#### Changed
* [!70](https://gitlab.com/energyincities/besos/-/merge_requests/70) Fix BESOS [readthedocs](https://besos.readthedocs.io/en/stable/) pages.  Stable = latest tagged release.  Latest = dev branch.

### [Version 1.6.0](https://gitlab.com/energyincities/besos/-/releases#1.6.0)
#### Added
*  [!39](https://gitlab.com/energyincities/besos/-/merge_requests/39)
 `keep_dirs` flag to `EvaluatorEP.df_apply`. It will cause each run's output to be stored
 in it's own directory. This directory's name is stored in a results column.  
*  [!39](https://gitlab.com/energyincities/besos/-/merge_requests/39)
 Evaluation functions for `EvaluatorGeneric` and `error_values` for all Evaluators can now represent their result as
a single tuple of all the objective and constraint values.
*  [!49](https://gitlab.com/energyincities/besos/-/merge_requests/49)
 Buildings with `HVACTemplate` objects are now supported. They will
 have the `--expandoutputs` flag added when running, preventing them from erroring out.
*  [!48](https://gitlab.com/energyincities/besos/-/merge_requests/48)
 A previous removal of the `epw_file` argument is now backwards compatible. Warnings
 will be emitted instead of errors when `EvaluatorEP` is initialised with this argument.

#### Changed
*  [!47](https://gitlab.com/energyincities/besos/-/merge_requests/47)
 `ParameterEH` should be replaced by a `Parameter` and
 a `PathSelector`. Examples updated accordingly
*  [!44](https://gitlab.com/energyincities/besos/-/merge_requests/44)
 Update Selector Documentation.
* [!57](https://gitlab.com/energyincities/besos/-/merge_requests/57)
 `eppySupport.get_idfobject_from_name` has been moved to `eppy_funcs.get_idfobject_from_name`
*  [59!](https://gitlab.com/energyincities/besos/-/merge_requests/59)
 The `get_building` and `get_idf` functions will warn instead of erroring when an idd is already set
 and no idd is provided on a subsequent call. (The error was from trying to change the idd to the default
 value.) If a new idd is provided, the functions will still raise an error.

#### Deprecated
*  [!47](https://gitlab.com/energyincities/besos/-/merge_requests/47)
 `ParameterEH` should be replaced by a `Parameter` and a `PathSelector`
*  [!39](https://gitlab.com/energyincities/besos/-/merge_requests/39)
 Evaluation functions for `EvaluatorGeneric` and `error_values` should use a single tuple of values rather than
 separate tuples for outputs and constraints.
* [!57](https://gitlab.com/energyincities/besos/-/merge_requests/57)
 All functions from `eppySupport` are now deprecated.
* [!62](https://gitlab.com/energyincities/besos/-/merge_requests/62)
  The `version` argument for `objectives.read_eso` is deprecated, since
  the correct version is now extracted automatically.
* [67!](https://gitlab.com/energyincities/besos/-/merge_requests/67)
  The version argument on `EvaluatorEP.__init__`, `eplus_funcs.run_building`
  and `eplus_funcs.print_available_outputs` is deprecated, since
  the correct version is now extracted automatically.
* [67!](https://gitlab.com/energyincities/besos/-/merge_requests/67)
  The `eplus_funcs.check_idf_version` function is deprecated, since using
  we use the correct version number automatically.


### [Version 1.5.0](https://gitlab.com/energyincities/besos/-/releases#1.5.0)
#### Added
*  Path for Bonmin to the 'rbf_opt' function so that 'rbf_opt' can be configured for a cluster
*  Random seed number to the 'rbf_opt' function

----

### [Version 1.4.3](https://gitlab.com/energyincities/besos/-/releases#1.4.3)
#### Added
*  Additional examples
*  DASK framework to run E+ jobs in parallel
*  Integrated Geomeppy - geometric edits to E+ files
*  Generic evaluator class
*  Daylight control features
*  Black formatter standard
#### Changed
*  Updated examples
*  Improved error handling
*  Energy Plus default version to 9.1.0
*  Window to wall ratio function can now be set via direction

----

### [Version 1.3.3](https://gitlab.com/energyincities/besos/-/releases#1.3.3)
#### Added
*  Energy Hub parameter class - 'ParameterEH'
#### Changed
*  bug fix to adaptive sampling class

----

### [Version 1.3.1](https://gitlab.com/energyincities/besos/-/releases#1.3.1)
#### Changed
* Additional fix to error output handling

----

### [Version 1.3.0](https://gitlab.com/energyincities/besos/-/releases#1.3.0)
#### Changed
*  Major bug fix to prevent overwriting files, redirection of E+ output files.

----

### [Version 1.2.3](https://gitlab.com/energyincities/besos/-/releases#v1.2.3)
#### Added
*  'adaptive_sampler_lv' sampling class
*  Getting Started PDF document
#### Changed
*  Documentation changes on [readthedocs](https://besos.readthedocs.io/en/latest/#)

----

### [Version 1.2.0](https://gitlab.com/energyincities/besos/-/releases#v1.2.0)
#### Added
* 'full_factorial' sampling function

----

### [Version 1.1.0](https://gitlab.com/energyincities/besos/-/releases#v1.1.0)
#### Added
*  EnergyHub Evaluator - EvaluatorEH object
#### Changed
*  Improved testing parameters and scope
*  File path restructuring
*  Various bug fixes

----

### [Version 1.0.0](https://gitlab.com/energyincities/besos/-/releases#v1.0.0)
*  Initial release of the BESOS repo, view examples and [readthedocs](https://besos.readthedocs.io/) for more information and documentation
*  Available on PyPI (`pip install besos`)
