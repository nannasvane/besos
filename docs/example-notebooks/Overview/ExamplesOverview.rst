Examples Overview
=================

This notebook gives an overview of the different example files and what
they are used for. They are organised following the folder structure of
the Examples directory. Click on the notebook titles to open them.

Surrogate Modelling
-------------------

`Interactive Surrogate <../../example-notebooks/How-to-Guides/InteractiveSurrogate.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

An overview of lots of BESOS functionality. We define an EnergyPlus
model with two parameters, generate samples that span the design space,
use these to train a surrogate model, then explore the design space
using an interactive plot that queries the surrogate model.

`Overview of Surrogate modelling on BESOS <../../example-notebooks/Overview/SMOverview.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This notebook gives an overview of surrogate modelling on BESOS, with
links to many other example notesbooks that demonstrate specific
functionality: - `Fitting a Gaussian Process
model <../../example-notebooks/How-to-Guides/FitGPModel.html>`__ - `Fitting a feed-forward
Neural Network <../../example-notebooks/How-to-Guides/FitFeedforwardNeuralNetwork.html>`__
- `Fitting a Neural-Network using
TensorFlow <../../example-notebooks/How-to-Guides/FitNNTF.html>`__ - `Using an adaptive
fitting algorithm <../../example-notebooks/How-to-Guides/FitGPAdaptive.html>`__

Optimization
------------

`Building Optimization <../../example-notebooks/How-to-Guides/BuildingOptimization.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This notebook gives an overview of building design optimization. It uses
EnergyPlus via an ``Evaluator`` and defines the optimization problem
using ``Parameters`` and ``Descriptors``.

`Optimization Run Flexibility <../../example-notebooks/Reference/OptimizationRunFlexibility.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

It is possible to change the configuration of the algorithm part way
through the optimisation process, or even to switch algorithms
completely. Doing so requires using Platypus algorithms directly,
instead of the algorithm wrappers provided through the optimization
module.

`Objectives and Constraints <../../example-notebooks/Reference/ObjectivesAndConstraints.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

There are two ways to use the outputs of an Evaluator: Objectives and
Constraints. These are both made using the MeterReader and
VariableReader classes.

Energy Hub
----------

`Overview of Energy Hub modelling on BESOS <../../example-notebooks/Overview/EHOverview.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This notebook gives an overview of the Energy Hub model, with links to
the following example notesbooks that demonstrate specific
functionality: - `Run EHub <../../example-notebooks/How-to-Guides/RunEHub.html>`__ is a simple
example that walks through the use of EHub using the original workflow.
- `Exploring Command Line Features of
EHub <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/ExploringCommandLine.ipynb>`__ explains the command line
features of the EHub model. - `Adding custom
constraints <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/CustomConstraints.ipynb>`__ shows how to add
three different types of custom constraints. - `Modify existing
constraint <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/ModifyExistingConstraint.ipynb>`__ edits an
existing constraint of the base model to add a subsidy for certain
renewable technologies. - `Remove existing
constraint <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/RemoveConstraint.ipynb>`__ turns off an existing
constraint. - `Rooftop Constraint <../../example-notebooks/Tutorials/LimitedArea.html>`__ adds
a constraint which limits roof size on which solar technologies can be
installed. - `Time Varying Grid
Price <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/TimeVaryingGridPrice.ipynb>`__ adds a time series which
represents grid price. - `Time Resolved Carbon
Factors <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/TimeResolvedCarbonFactors.ipynb>`__ combines Energy
Hub modelling in BESOS with analysis of the wider power system using
SILVER. - `Storage plots <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/StoragePlots.ipynb>`__ gives
graphical results showing how a storage device is operated. -
`Overriding Input Data <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/OverridingInputData.ipynb>`__ gets
load data for some typical days from a separate file, overwrites the
time series and solves the model for each time series. - `Multiple
Hubs <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/Networks/MultipleHubs.ipynb>`__ gives an overview of how
hubs are linked together. - `Linear
Powerflow <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/Networks/LinearPowerflow.ipynb>`__ adds linear
powerflow constraints to the Energy Hub model. - `Edit
Excel <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/EditExcel.ipynb>`__ is a notebook to edit the input
excel file. - `Edit Networks
Excel <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/EditNetworksExcel.ipynb>`__ is a notebook to edit the
networks excel file. - `EHEvaluator <../../example-notebooks/How-to-Guides/EHEvaluator.html>`__
demonstrates the EHEvaulator. - `EP to EH
Evaluators <../../example-notebooks/How-to-Guides/EPtoEHEvaluators.html>`__ shows how to combine an
Energy Hub and and EnergyPlus Evaluator. - `PyEHub Parameter
Editor <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/ParameterEditor.ipynb>`__ shows how to define BESOS
Parameters for PyEHub models.

Data Analysis
-------------

`Bayesian Network <../../example-notebooks/How-to-Guides/BayesianNetwork.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A stochastic generator of hourly electricity use in a residential
building fitted to measured data, to generate synthetic profiles for a
Monte Carlo analysis.

`Fitting Grey-box Models <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/DataAnalysis/ecobee_public/FittingGreyboxModels.ipynb>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This notebook and associated files implement the methods used in a paper
applying grey-box models to building temperature data to obtain building
characteristics. It also shows how notebooks can be used as the
front-end for scripts written as pure Python .py files.

Parametrics and Sensitivity Analysis
------------------------------------

`Parametric Analysis <../../example-notebooks/How-to-Guides/ParametricAnalysis.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

How to define a parametric (brute-force) analysis using an EnergyPlus
model, and get the energy use for all combinations of parameter values.

`Morris Screening <../../example-notebooks/How-to-Guides/MorrisScreening.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Implementation of the Morris screening global sensitivity method.

`Sobol Sensitivity Analysis <../../example-notebooks/How-to-Guides/SobolSensitivityAnalysis.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Implementation of the Sobol global variance-based sensitivity analysis
method.

Evaluators
----------

.. _evaluators-1:

`Evaluators <../../example-notebooks/Reference/Evaluators.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

How to use the three BESOS evaluators: - the EnergyPlus Evaluator
(``EvaluatorEP``) - the Generic Evaluator (``EvaluatorGeneric``) - the
Energy Hub Evaluator (``EvaluatorEH``)

Example of how to use EvaluatorGeneric to wrap multiple evaluators or
functions for multiple objectives problem.

`Descriptors <../../example-notebooks/Reference/Descriptors.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Descriptors specify what kinds of values are valid for a parameter.

`Selectors <../../example-notebooks/Reference/Selectors.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Selectors identify which part of a model to modify for a given
parameter, and how to modify it.

`Generic Selectors <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/Evaluators/Generic%20Selectors.ipynb>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Generic Selectors let you provide a function which modifies the
building, for when you need more customisation than other selectors
provide.

`Custom EnergyPlus Evaluator functions <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/Evaluators/CustomEPObjective.ipynb>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Custom functions can be executed on the EnergyPlus results.

`Running EnergyPlus directly <../../example-notebooks/Reference/RunEPlus.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Various ways of running EnergyPlus directly and interacting with models
via EPPy.

`Automatic Error Handling <../../example-notebooks/Reference/AutomaticErrorHandling.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Sometimes there are parts of the design space that we want to explore
that will cause the EnergyPlus simulation to fail, such as invalid
combinations of parameter values. In this example, we demonstrate how
this is handled using an undefined material to represent an invalid
state.

`Different Version of EnergyPlus <../../example-notebooks/Reference/DifferentVersionEP.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Use different version of EnergyPlus to run simulation.

Cluster computing
-----------------

`Cluster Submission <../../example-notebooks/How-to-Guides/ClusterSubmission.html>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Job submission to the Compute Canada cloud or similar can be integrated
into the notebook workflow, as seen here. Also provided are tutorials on
`Linux terminal commands <../../example-notebooks/Tutorials/TerminalTutorial.html>`__
and `SFTP file transfer <../../example-notebooks/How-to-Guides/SFTP.html>`__.

Other
-----

`Geomeppy <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/Geomeppy/Geomeppy.ipynb>`__
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some basic examples of geomeppy library.
