Overview of Energy Hub modelling on BESOS
=========================================

The Energy Hub model implemented in Python in the `PyEHub
repository <https://gitlab.com/energyincities/python-ehub>`__ optimizes
operational energy balancing and desig selection and sizing of an energy
system given technical, economical and environmental constraints
[`1 <https://www.sciencedirect.com/science/article/pii/S0142061512000701>`__].
The module is provided as part of BESOS, where a Jupyter Notebook can be
used to set up a basic model, customise it, run it and plot the results.
A series of examples are given below.

Basic workflow
--------------

The figure below shows the original EHub workflow to run a model defined
in an Excel file which describes the system layout, convertors, storage
system(s), energy streams and timeseries. Several `tutorials and
examples <https://gitlab.com/energyincities/python-ehub/tree/master/pyehub/tutorials>`__
are available through GitLab on how to setup up this input file. This
gets parsed into model data, which is used to set up the problem
constraints, which are passed to a solver like CPlex or GLPK to find the
least cost solution. This solution is printed to the terminal and to an
output Excel file.

*Simple PyEHub workflow.*

-  `Run EHub <../../example-notebooks/How-to-Guides/RunEHub.html>`__ is a simple example that walks through
   the use of EHub using the original workflow.
-  `Exploring Command Line Features of
   EHub <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/ExploringCommandLine.ipynb>`__ explains the command line
   features of the EHub model.

Using PyEHub features via Jupyter Notebooks
-------------------------------------------

The figure below shows the workflow on BESOS. We make use of Jupyter
Notebooks to make changes to the model definition file, extend the base
PyEHub code, run model variations, read the outputs into a pandas
``DataFrame`` and plot them.

*Jupyter Notebook based workflow for Energy Hub modelling on BESOS.*

The following notebooks give examples of interacting with Energy Hub
model from a notebook. The core model is customised in various ways,
then applied to an Excel-based model file.

-  `Adding custom constraints <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/CustomConstraints.ipynb>`__ shows how to
   add three different types of custom constraints.
-  `Modify existing constraint <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/ModifyExistingConstraint.ipynb>`__ edits
   an existing constraint of the base model to add a subsidy for certain
   renewable technologies.
-  `Remove existing constraint <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/RemoveConstraint.ipynb>`__ turns off an
   existing constraint.
-  `Rooftop Constraint <../../example-notebooks/Tutorials/LimitedArea.html>`__ adds a constraint which
   limits roof size on which solar technologies can be installed.
-  `Time Varying Grid Price <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/TimeVaryingGridPrice.ipynb>`__ adds a time
   series which represents grid price.
-  `Time Resolved Carbon Factors <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/TimeResolvedCarbonFactors.ipynb>`__
   combines Energy Hub modelling in BESOS with analysis of the wider
   power system using SILVER.
-  `Storage plots <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/StoragePlots.ipynb>`__ gives graphical results
   showing how a storage device is operated.
-  `Overriding Input Data <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/OverridingInputData.ipynb>`__ gets load data
   for some typical days from a separate file, overwrites the time
   series and solves the model for each time series. Outputs are stored
   in a single ``DataFrame`` and key items are printed.
-  `Workshop - Remote Communities <../../example-notebooks/Tutorials/Workshop_RemoteCommunities.html>`__
   outlines the process of modelling energy systems for remote
   communities. This includes reading data, optimizing parameters, and
   visualizing the results

Multiple Hubs
-------------

The Energy Hub model can provide useful results for just one hub, but
the real power of the model is when multiple hubs are linked together. -
`Multiple Hubs <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/Networks/MultipleHubs.ipynb>`__ gives an overview of how
hubs are linked together. - `Linear
Powerflow <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/Networks/LinearPowerflow.ipynb>`__ adds linear powerflow
constraints to the Energy Hub model.

Editing the Excel files in Notebooks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Excel file(s) that define a model can also be edited from a
notebook. - `Edit Excel <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/EditExcel.ipynb>`__ is a notebook to edit the
input excel file. - `Edit Networks Excel <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/EditNetworksExcel.ipynb>`__ is
a notebook to edit the networks excel file.

EHEvaluator and Parameter Editor
--------------------------------

Several functions are provided for integration with the rest of BESOS.

The ``EHEvaluator`` allows an Energy Hub model to be called via the
BESOS ``Evaluator`` class. This is useful for integration with the
EnergyPlus and Surrogate Modelling Evaluators. -
`EHEvaluator <../../example-notebooks/How-to-Guides/EHEvaluator.html>`__ demonstrates the EHEvaulator. - `EP
to EH Evaluators <../../example-notebooks/How-to-Guides/EPtoEHEvaluators.html>`__ shows how to combine an
Energy Hub and and EnergyPlus Evaluator.

The ``pyehub_parameter_editor`` function is used by the ``EHEvaluator``
for defining ``Parameters``. - `PyEHub Parameter
Editor <https://gitlab.com/energyincities/besos-examples/-/tree/master/besos/examples/EnergyHub/ParameterEditor.ipynb>`__ shows how to define BESOS Parameters
for PyEHub models.
