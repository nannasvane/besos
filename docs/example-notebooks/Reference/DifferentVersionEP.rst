Working with different versions of EnergyPlus
---------------------------------------------

Since besos is using the absolute path, if you are running besos on your
computer, make sure you install the EnergyPlus under:

Windows: C:/EnergyPlusV{version}

Linux: /usr/local/EnergyPlus-{version}

Mac: /Applications/EnergyPlus-{version}

or specify in evaluators the EnergyPlus path in 'ep\_path' argument

.. code:: ipython3

    from besos import eppy_funcs as ef
    from besos.evaluator import EvaluatorEP
    from besos.parameters import FieldSelector, Parameter
    from besos.problem import EPProblem
    from besos.eplus_funcs import get_idf_version

    import config

The version of EnergyPlus to run is determined on the version of idf
that is passed in. In this case, the default building is version 9-0-1.

.. code:: ipython3

    building = ef.get_building(mode="idf")
    get_idf_version(building)




.. parsed-literal::

    '9.0'



We now add parameters and objectives to building

.. code:: ipython3

    parameters = [
        Parameter(
            FieldSelector(object_name="Mass NonRes Wall Insulation", field_name="Thickness")
        )
    ]

    objectives = ["Electricity:Facility", "Gas:Facility"]
    problem = EPProblem(parameters, objectives)

We can also set the version of energyplus that we want to use for
simulation by passing the version parameter to the EvaluatorEP.

The program will show a warning if the version in the building file
doesn't match the input version.

.. code:: ipython3

    evaluator = EvaluatorEP(problem, building)
    result = evaluator([0.5])  # run the evaluator with wall insulation thickness set to 0.5m
    values = dict(zip(objectives, result))
    for key, value in values.items():
        print(key, " :: ", "{0:.2f}".format(value / 3.6e6), "kWh")


.. parsed-literal::

    Electricity:Facility  ::  505.20 kWh
    Gas:Facility  ::  603.35 kWh
