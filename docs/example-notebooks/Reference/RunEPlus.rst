Running EnergyPlus directly
===========================

Here we demonstrate various ways of interacting with EnergyPlus models
directly.

.. code:: ipython3

    import os

    from besos import eppy_funcs as ef
    from besos.evaluator import EvaluatorEP
    from besos.problem import EPProblem

Using an Evaluator with no Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Here we run in.idf and sum the Meter ``Electricity:Facility``, but don't
vary any parameters.

.. code:: ipython3

    building = ef.get_building("in.idf")
    evaluator = EvaluatorEP(EPProblem(outputs=["Electricity:Facility"]), building)
    evaluator([])




.. parsed-literal::

    (6029872434.744861,)



Using EPPy
~~~~~~~~~~

BESOS building objects are actually EPPy building objects. `Eppy's
documentation <https://eppy.readthedocs.io/en/latest/>`__ describes how
to explore and modify the IDF object directly. Here we list the
materials present in in.idf, then run it.

.. code:: ipython3

    building = ef.get_building("in.idf")
    building.savecopy("in_copy.idf")
    print([materials.Name for materials in building.idfobjects["MATERIAL"]])
    building.epw = "./weatherfile.epw"
    try:
        building.run(output_directory="output_folder")
    finally:
        os.rename("in_copy.idf", "in.idf")


.. parsed-literal::

    ['1/2IN Gypsum', '1IN Stucco', '8IN Concrete HW', 'F08 Metal surface', 'F16 Acoustic tile', 'G01a 19mm gypsum board', 'G05 25mm wood', 'I01 25mm insulation board', 'M11 100mm lightweight concrete', 'MAT-CC05 4 HW CONCRETE', 'Metal Decking', 'Roof Insulation [18]', 'Roof Membrane', 'Wall Insulation [31]']

    /usr/local/EnergyPlus-9-0-1/energyplus --weather /home/user/repos/workingBesos/besos/docs/besos-examples/besos/examples/EnergyPlus/weatherfile.epw --output-directory /home/user/repos/workingBesos/besos/docs/besos-examples/besos/examples/EnergyPlus/output_folder --idd /usr/local/EnergyPlus-9-0-1/Energy+.idd /home/user/repos/workingBesos/besos/docs/besos-examples/besos/examples/EnergyPlus/in.idf



Using a bash cell to execute it from the command line
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run ``energyplus -h`` for more commands.

.. code:: ipython3

    !energyplus -w weatherfile.epw in.idf


.. parsed-literal::

    EnergyPlus Starting
    EnergyPlus, Version 9.3.0-baff08990c, YMD=2021.02.09 22:57
    **FATAL:Errors occurred on processing input file. Preceding condition(s) cause termination.
    EnergyPlus Run Time=00hr 00min  0.19sec
    Program terminated: EnergyPlus Terminated--Error(s) Detected.
