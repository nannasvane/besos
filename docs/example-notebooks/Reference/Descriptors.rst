Descriptors
===========

| Descriptors specify what kinds of values are valid for a parameter.
  There are currently, 2 variants: ``RangeParameter`` and
  ``CategoryParameter``.
| (Note that even though their names end with Parameter, they are
  actually descriptors, not parameters.)

.. code:: ipython3


    from besos.parameters import (
        RangeParameter,
        CategoryParameter,
        Parameter,
    )
    from besos.problem import Problem
    from besos import sampling
    from besos.evaluator import EvaluatorGeneric

RangeParameters
~~~~~~~~~~~~~~~

:math:`min \leq x \leq max`

.. code:: ipython3

    zero_to_one_exclusive = RangeParameter(min_val=0.01, max_val=0.99)

CategoryParameters
~~~~~~~~~~~~~~~~~~

A list of options.

.. code:: ipython3

    text_example = CategoryParameter(options=["a", "b", "c", "other"])
    single_digit_integers = CategoryParameter(options=range(10))

Sampling
~~~~~~~~

These descriptors can be used to make ``Parameters``. Then we can
generate samples.

.. code:: ipython3

    parameters = [
        Parameter(value_descriptors=zero_to_one_exclusive, name="0-1"),
        Parameter(value_descriptors=single_digit_integers, name="single digit"),
        Parameter(value_descriptors=text_example, name="text"),
    ]
    problem = Problem(parameters, outputs=["output"])

    samples = sampling.dist_sampler(sampling.lhs, problem, num_samples=10)
    samples




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0-1</th>
          <th>single digit</th>
          <th>text</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.731019</td>
          <td>3</td>
          <td>other</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.445608</td>
          <td>5</td>
          <td>b</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.347260</td>
          <td>6</td>
          <td>b</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.182080</td>
          <td>9</td>
          <td>a</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.213295</td>
          <td>2</td>
          <td>c</td>
        </tr>
        <tr>
          <th>5</th>
          <td>0.896126</td>
          <td>1</td>
          <td>c</td>
        </tr>
        <tr>
          <th>6</th>
          <td>0.578084</td>
          <td>7</td>
          <td>a</td>
        </tr>
        <tr>
          <th>7</th>
          <td>0.067447</td>
          <td>4</td>
          <td>other</td>
        </tr>
        <tr>
          <th>8</th>
          <td>0.811553</td>
          <td>8</td>
          <td>a</td>
        </tr>
        <tr>
          <th>9</th>
          <td>0.647950</td>
          <td>0</td>
          <td>other</td>
        </tr>
      </tbody>
    </table>
    </div>



Evaluation
~~~~~~~~~~

Since we did not specify selectors for the parameters, we cannot
evaluate them using an EnergyPlus simulation. Instead, we will use a
custom evaluation function.

.. code:: ipython3

    def evaluation_function(values):
        x, y, z = values
        if z == "other":
            return (x,)
        else:
            return (x * y,)


    evaluator = EvaluatorGeneric(evaluation_function, problem)
    # The evaluator will use this objective by default
    outputs = evaluator.df_apply(samples, keep_input=True)
    # outputs is a pandas dataframe with one column since only one objective was requested



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=10.0, style=ProgressStyle(description_wid…


.. parsed-literal::




.. code:: ipython3

    outputs




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0-1</th>
          <th>single digit</th>
          <th>text</th>
          <th>output</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.731019</td>
          <td>3</td>
          <td>other</td>
          <td>0.731019</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.445608</td>
          <td>5</td>
          <td>b</td>
          <td>2.228040</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.347260</td>
          <td>6</td>
          <td>b</td>
          <td>2.083560</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.182080</td>
          <td>9</td>
          <td>a</td>
          <td>1.638716</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.213295</td>
          <td>2</td>
          <td>c</td>
          <td>0.426590</td>
        </tr>
        <tr>
          <th>5</th>
          <td>0.896126</td>
          <td>1</td>
          <td>c</td>
          <td>0.896126</td>
        </tr>
        <tr>
          <th>6</th>
          <td>0.578084</td>
          <td>7</td>
          <td>a</td>
          <td>4.046588</td>
        </tr>
        <tr>
          <th>7</th>
          <td>0.067447</td>
          <td>4</td>
          <td>other</td>
          <td>0.067447</td>
        </tr>
        <tr>
          <th>8</th>
          <td>0.811553</td>
          <td>8</td>
          <td>a</td>
          <td>6.492422</td>
        </tr>
        <tr>
          <th>9</th>
          <td>0.647950</td>
          <td>0</td>
          <td>other</td>
          <td>0.647950</td>
        </tr>
      </tbody>
    </table>
    </div>
