Common Linux terminal commands
==============================

This notebook lists common commands used on the Linux terminal.

``Right-click``
~~~~~~~~~~~~~~~

Jupyter’s terminals work with ``Shift + Right-click``. *This is
important when you want to paste something copied earlier.*

For sftp
~~~~~~~~

-  If you are inside of ``sftp>`` prompt, the usual commands are
   executed on the remote machine and the ones in parentheses are
   executed on local machine.
-  The commands which are not succeeded by parentheses cannot be
   executed locally (with ``l`` as a prefix) from ``sftp>`` prompt.
-  To run any command from inside of the ``sftp>`` prompt, precede it
   with a ``!``. You can do it for the ones that work with ``l`` as a
   prefix too.

Try out the commands on the following terminal:

.. code:: ipython3

    from IPython.display import IFrame


    your_gitlab_username = "mdrpanwar"  # change this to your username
    IFrame(
        "https://hub.besos.uvic.ca/user/" + your_gitlab_username + "/terminals/3",
        width=1200,
        height=250,
    )




.. raw:: html


    <iframe
        width="1200"
        height="250"
        src="https://hub.besos.uvic.ca/user/mdrpanwar/terminals/3"
        frameborder="0"
        allowfullscreen
    ></iframe>




pwd (or lpwd)
-------------

-  Usage: ``pwd``
-  Used to get the current working directory(CWD). We will use the term
   CWD time and again. It means the directory your terminal is in right
   now. All commands are executed inside this directory.

ls (or lls)
-----------

-  Usage: ``ls``
-  ‘lists’ out the directories and files in the CWD [*Hidden
   directories/files are not shown.*]
-  Use ``ls -a`` to list out even the hidden directories/files.

mkdir (or lmkdir)
-----------------

-  Usage: ``mkdir dir_name``
-  To make a directory named ``dir_name`` in the CWD.

cd (or lcd)
-----------

-  Usage: ``cd dir_name``

-  ‘Changes CWD’ to the directory named ``dir_name``. Here ``dir_name``
   must be a directory residing inside the current working directory or
   a path starting within CWD.

-  Every directory in linux contains ``.`` and ``..``, which refers to
   the ``current working directory`` and
   ``parent of current working directory`` respectively.

-  Use ``cd ..`` to navigate to the parent of the CWD.

-  .. rubric:: Consider the following directory structure:
      :name: consider-the-following-directory-structure

-  parent

   -  inparent

      -  inchild

         -  simple.out
         -  a.out
         -  b.out

      -  inchild1

         -  incurrentdir
         -  c.out

   -  .. rubric:: inparent2
         :name: inparent2

-  Examples (from inside of ``parent/inparent/inchild1``):

   -  ``cd ../inparent/inchild``
   -  ``cd incurrentdir``

rm
--

-  Usage: ``rm filename_1 filename_2 ... filename_n``
-  To delete the files named ``filename_1``, ``filename_2``, ….,
   ``filename_n`` from the CWD.
-  To delete a directory, use ``rm -r dir_name`` where ``dir_name`` is a
   directory residing in CWD or a path to a diretory.
-  Instead of being name of files in CWD, ``filename_i``\ can also be a
   path to a file.
-  Examples (from inside of ``parent/inparent/inchild``):

   -  ``rm simple.out``
   -  ``rm a.out b.out``
   -  ``rm ../inchild1/c.out``

mv
--

-  Usage: ``mv filename_1 filename_2 ... filename_n dir_name``
-  To move ``filename_1``, ``filename_2``, …., ``filename_n`` to
   directory ``dir_name``.
-  Instead of being name of file in CWD, ``filename_i`` can also be a
   path to a file.
-  Instead of being name of directory in CWD, ``dir_name``\ can also be
   a path to a directory.
-  Examples (from inside of ``parent/inparent/inchild1``):

   -  ``mv c.out incurrentdir``
   -  ``mv ../inchild/a.out ../../inparent2``

cp
--

-  Usage: ``cp filename_1 filename_2 ... filename_n dir_name``
-  To copy ``filename_1``, ``filename_2``, …., ``filename_n`` to
   ``dir_name`` directory.
-  To copy a directory ``dir_name1`` into directory ``dir_name2``, use
   ``cp -r dir_name1 dir_name2`` where ``dir_name1`` and ``dir_name2``
   are directories residing in CWD or paths to directories..
-  Instead of being name of file in CWD, ``filename_i`` can also be a
   path to a file.
-  Instead of being name of directory in CWD, ``dir_name``\ can also be
   a path to a directory.
-  Examples (from inside of ``parent/inparent/inchild1``):

   -  ``cp c.out incurrentdir``
   -  ``cp ../inchild/a.out ../../inparent2``


top
---

-  Usage: ``top``
-  Lists out all the running processes with the resourses being used by
   each process.
-  The output is similar to that of ``Task Manager`` on Windows.
