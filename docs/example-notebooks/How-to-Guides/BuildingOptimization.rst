Building Optimization
=====================

This notebook performs building design optimization using EnergyPlus and
BESOS helper functions. We load a model from in.idf, define parameters
to vary, set objectives, test the model, then run a multi-objective
genetic algorithm and plot the optimized designs.

Import libraries
~~~~~~~~~~~~~~~~

.. code:: ipython3

    import pandas as pd
    from besos import eppy_funcs as ef, sampling
    from besos.evaluator import EvaluatorEP
    from besos.optimizer import NSGAII, df_solution_to_solutions
    from besos.parameters import RangeParameter, expand_plist, wwr
    from besos.problem import EPProblem
    from matplotlib import pyplot as plt
    from platypus import Archive, Hypervolume, Solution

Load the base EnergyPlus .idf file
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    building = ef.get_building("in.idf")

Define design parameters and ranges
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Define a parameter list using a helper function, in this case building
orientation and window-to-wall ratio.

.. code:: ipython3

    parameters = []
    parameters = expand_plist(
        {"Building 1": {"North Axis": (0, 359)}}  # Name from IDF Building object
    )

    parameters.append(
        wwr(RangeParameter(0.1, 0.9))
    )  # Add window-to-wall ratio as a parameter between 0.1 and 0.9 using a custom function

Objectives
~~~~~~~~~~

Using Heating and Cooling energy outputs as simulation objectives, make
a problem instance from these parameters and objectives.

.. code:: ipython3

    objectives = ["DistrictCooling:Facility", "DistrictHeating:Facility"]
    besos_problem = EPProblem(parameters, objectives)

Set up EnergyPlus evaluator object to run simulations for this building and problem
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    evaluator = EvaluatorEP(
        besos_problem, building, out_dir="outputdir", err_dir="outputdir"
    )  # outputdir must exist; E+ files will be written there
    runs = pd.DataFrame.from_dict(
        {"0": [180, 0.5]}, orient="index"
    )  # Make a dataframe of runs with one entry for South and 50% glazing
    outputs = evaluator.df_apply(runs)  # Run this as a test
    outputs



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=1.0, style=ProgressStyle(description_widt…


.. parsed-literal::






.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>DistrictCooling:Facility</th>
          <th>DistrictHeating:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>3.233564e+09</td>
          <td>4.931726e+09</td>
        </tr>
      </tbody>
    </table>
    </div>



Run the Genetic Algorithm
-------------------------

Run the optimizer using this evaluator for a population size of 20 for
10 generations.

.. code:: ipython3

    results = NSGAII(evaluator, evaluations=10, population_size=20)
    results




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>North Axis</th>
          <th>RangeParameter [0.1, 0.9]</th>
          <th>DistrictCooling:Facility</th>
          <th>DistrictHeating:Facility</th>
          <th>violation</th>
          <th>pareto-optimal</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>263.141621</td>
          <td>0.129692</td>
          <td>4.295335e+09</td>
          <td>2.518508e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>1</th>
          <td>292.002451</td>
          <td>0.227006</td>
          <td>4.463966e+09</td>
          <td>2.689957e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>2</th>
          <td>339.185394</td>
          <td>0.683410</td>
          <td>4.385552e+09</td>
          <td>4.164144e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>3</th>
          <td>299.642312</td>
          <td>0.305285</td>
          <td>4.484553e+09</td>
          <td>2.958204e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>4</th>
          <td>28.829169</td>
          <td>0.280137</td>
          <td>4.322465e+09</td>
          <td>2.577191e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>5</th>
          <td>193.942624</td>
          <td>0.632927</td>
          <td>3.270069e+09</td>
          <td>5.663412e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>6</th>
          <td>284.171269</td>
          <td>0.423629</td>
          <td>4.453051e+09</td>
          <td>3.709380e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>7</th>
          <td>201.004448</td>
          <td>0.205321</td>
          <td>3.491288e+09</td>
          <td>3.299567e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>8</th>
          <td>222.356347</td>
          <td>0.838719</td>
          <td>3.679302e+09</td>
          <td>6.640699e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>9</th>
          <td>300.243976</td>
          <td>0.727996</td>
          <td>4.598198e+09</td>
          <td>4.884347e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>10</th>
          <td>131.502074</td>
          <td>0.272879</td>
          <td>3.756134e+09</td>
          <td>3.589675e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>11</th>
          <td>317.780853</td>
          <td>0.243146</td>
          <td>4.464903e+09</td>
          <td>2.490665e+09</td>
          <td>0</td>
          <td>True</td>
        </tr>
        <tr>
          <th>12</th>
          <td>317.265378</td>
          <td>0.642623</td>
          <td>4.529079e+09</td>
          <td>4.232315e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>13</th>
          <td>78.522690</td>
          <td>0.536158</td>
          <td>4.345231e+09</td>
          <td>4.379867e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>14</th>
          <td>300.109981</td>
          <td>0.594342</td>
          <td>4.547690e+09</td>
          <td>4.280460e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>15</th>
          <td>233.827874</td>
          <td>0.249924</td>
          <td>3.905637e+09</td>
          <td>3.391033e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>16</th>
          <td>4.219275</td>
          <td>0.607486</td>
          <td>4.256356e+09</td>
          <td>3.775692e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>17</th>
          <td>350.548384</td>
          <td>0.494473</td>
          <td>4.280642e+09</td>
          <td>3.334591e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>18</th>
          <td>60.838750</td>
          <td>0.874669</td>
          <td>4.558749e+09</td>
          <td>5.692981e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
        <tr>
          <th>19</th>
          <td>311.211108</td>
          <td>0.498106</td>
          <td>4.510503e+09</td>
          <td>3.688458e+09</td>
          <td>0</td>
          <td>False</td>
        </tr>
      </tbody>
    </table>
    </div>



Visualize the results
~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    optres = results.loc[
        results["pareto-optimal"] == True, :
    ]  # Get only the optimal results
    plt.plot(
        results["DistrictCooling:Facility"], results["DistrictHeating:Facility"], "x"
    )  # Plot all results in the background as blue crosses
    plt.plot(
        optres["DistrictCooling:Facility"], optres["DistrictHeating:Facility"], "ro"
    )  # Plot optimal results in red
    plt.xlabel("Cooling demand")
    plt.ylabel("Heating demand")




.. parsed-literal::

    Text(0, 0.5, 'Heating demand')




.. image:: BuildingOptimization_files/BuildingOptimization_15_1.png


.. code:: ipython3

    optres = optres.sort_values("DistrictCooling:Facility")  # Sort by the first objective
    optresplot = optres.drop(columns="violation")  # Remove the constraint violation column
    ax = optresplot.plot.bar(
        subplots=True, legend=None, figsize=(10, 10)
    )  # Plot the variable values of each of the optimal solutions


.. parsed-literal::

    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:307: MatplotlibDeprecationWarning:
    The rowNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().rowspan.start instead.
      layout[ax.rowNum, ax.colNum] = ax.get_visible()
    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:307: MatplotlibDeprecationWarning:
    The colNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().colspan.start instead.
      layout[ax.rowNum, ax.colNum] = ax.get_visible()
    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:313: MatplotlibDeprecationWarning:
    The rowNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().rowspan.start instead.
      if not layout[ax.rowNum + 1, ax.colNum]:
    /usr/local/lib/python3.7/dist-packages/pandas/plotting/_matplotlib/tools.py:313: MatplotlibDeprecationWarning:
    The colNum attribute was deprecated in Matplotlib 3.2 and will be removed two minor releases later. Use ax.get_subplotspec().colspan.start instead.
      if not layout[ax.rowNum + 1, ax.colNum]:



.. image:: BuildingOptimization_files/BuildingOptimization_16_1.png


Hypervolume
~~~~~~~~~~~

Now that initial results have been produced and verified against
expectations, enlarge evaluations and population size to produce
increased optimization of results.

.. code:: ipython3

    results_2 = NSGAII(evaluator, evaluations=20, population_size=50)

Compare first run and second run
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    optres_2 = results_2.loc[results_2["pareto-optimal"] == True, :]
    plt.plot(
        optres["DistrictCooling:Facility"], optres["DistrictHeating:Facility"], "bo"
    )  # Plot first optimal results in blue
    plt.plot(
        optres_2["DistrictCooling:Facility"], optres_2["DistrictHeating:Facility"], "ro"
    )  # Plot second optimal results in red
    plt.xlabel("Cooling demand")
    plt.ylabel("Heating demand")




.. parsed-literal::

    Text(0, 0.5, 'Heating demand')




.. image:: BuildingOptimization_files/BuildingOptimization_21_1.png


Calculate the hypervolume
~~~~~~~~~~~~~~~~~~~~~~~~~

.. code:: ipython3

    reference_set = Archive()
    platypus_problem = evaluator.to_platypus()
    for _ in range(20):
        solution = Solution(platypus_problem)
        solution.variables = sampling.dist_sampler(
            sampling.lhs, besos_problem, 1
        ).values.tolist()[0]
        solution.evaluate()
        reference_set.add(solution)

    hyp = Hypervolume(reference_set)
    print(
        "Hypervolume for result 1:",
        hyp.calculate(df_solution_to_solutions(results, platypus_problem, besos_problem)),
    )
    print(
        "Hypervolume for result 2:",
        hyp.calculate(df_solution_to_solutions(results_2, platypus_problem, besos_problem)),
    )


.. parsed-literal::

    Hypervolume for result 1: 0.27192093845955767
    Hypervolume for result 2: 0.23978050252228306
