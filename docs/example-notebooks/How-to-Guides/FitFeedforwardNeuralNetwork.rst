Fit feedforward Neural Network model
====================================

.. code:: ipython3

    import warnings

    import chart_studio
    from besos import eppy_funcs as ef, sampling
    from besos.evaluator import EvaluatorEP, EvaluatorGeneric
    from besos.problem import EPProblem
    from chart_studio import plotly as py
    from plotly import graph_objs as go
    from sklearn.model_selection import GridSearchCV, train_test_split
    from sklearn.neural_network import MLPRegressor
    from sklearn.preprocessing import StandardScaler

    from parameter_sets import parameter_set

We begin by: + getting a predefined list of 7 parameters from
``parameter_sets.py`` + making these into a ``problem`` with electricty
use as the objective + and making an ``evaluator`` using the default
EnergyPlus building.

.. code:: ipython3

    parameters = parameter_set(7)
    problem = EPProblem(parameters, ["Electricity:Facility"])
    building = ef.get_building()
    evaluator = EvaluatorEP(problem, building)

Then we get 20 samples across this design space and evaluate them.

.. code:: ipython3

    inputs = sampling.dist_sampler(sampling.lhs, problem, 20)
    outputs = evaluator.df_apply(inputs)
    inputs



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=20.0, style=ProgressStyle(description_wid…


.. parsed-literal::






.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Conductivity</th>
          <th>Thickness</th>
          <th>U-Factor</th>
          <th>Solar Heat Gain Coefficient</th>
          <th>ElectricEquipment</th>
          <th>Lights</th>
          <th>Window to Wall Ratio</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.165451</td>
          <td>0.150655</td>
          <td>1.543389</td>
          <td>0.759667</td>
          <td>10.846373</td>
          <td>14.112927</td>
          <td>0.024352</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.132154</td>
          <td>0.147326</td>
          <td>4.990325</td>
          <td>0.476966</td>
          <td>12.153939</td>
          <td>13.535152</td>
          <td>0.253947</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.080456</td>
          <td>0.190067</td>
          <td>2.774125</td>
          <td>0.715199</td>
          <td>10.749949</td>
          <td>11.099823</td>
          <td>0.929049</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.033565</td>
          <td>0.287872</td>
          <td>0.251303</td>
          <td>0.886388</td>
          <td>12.717156</td>
          <td>14.578163</td>
          <td>0.122789</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.118116</td>
          <td>0.162183</td>
          <td>3.689406</td>
          <td>0.619588</td>
          <td>10.248964</td>
          <td>13.919604</td>
          <td>0.822831</td>
        </tr>
        <tr>
          <th>5</th>
          <td>0.104531</td>
          <td>0.127132</td>
          <td>4.077076</td>
          <td>0.528881</td>
          <td>12.324394</td>
          <td>14.488989</td>
          <td>0.670414</td>
        </tr>
        <tr>
          <th>6</th>
          <td>0.196692</td>
          <td>0.139876</td>
          <td>0.506217</td>
          <td>0.030411</td>
          <td>11.374182</td>
          <td>11.665950</td>
          <td>0.375758</td>
        </tr>
        <tr>
          <th>7</th>
          <td>0.144890</td>
          <td>0.267075</td>
          <td>4.432675</td>
          <td>0.172185</td>
          <td>13.053027</td>
          <td>10.926282</td>
          <td>0.750233</td>
        </tr>
        <tr>
          <th>8</th>
          <td>0.085398</td>
          <td>0.102225</td>
          <td>1.615485</td>
          <td>0.298316</td>
          <td>10.455626</td>
          <td>12.998602</td>
          <td>0.455249</td>
        </tr>
        <tr>
          <th>9</th>
          <td>0.188407</td>
          <td>0.252228</td>
          <td>2.493945</td>
          <td>0.833956</td>
          <td>14.747260</td>
          <td>12.044832</td>
          <td>0.615270</td>
        </tr>
        <tr>
          <th>10</th>
          <td>0.043554</td>
          <td>0.244529</td>
          <td>0.894356</td>
          <td>0.324217</td>
          <td>12.875479</td>
          <td>10.282988</td>
          <td>0.196747</td>
        </tr>
        <tr>
          <th>11</th>
          <td>0.176928</td>
          <td>0.180441</td>
          <td>2.198896</td>
          <td>0.669148</td>
          <td>14.493702</td>
          <td>13.226416</td>
          <td>0.343713</td>
        </tr>
        <tr>
          <th>12</th>
          <td>0.122820</td>
          <td>0.110766</td>
          <td>2.953618</td>
          <td>0.929649</td>
          <td>14.949231</td>
          <td>10.096406</td>
          <td>0.529761</td>
        </tr>
        <tr>
          <th>13</th>
          <td>0.155939</td>
          <td>0.237080</td>
          <td>0.744055</td>
          <td>0.102866</td>
          <td>13.478119</td>
          <td>12.425914</td>
          <td>0.968424</td>
        </tr>
        <tr>
          <th>14</th>
          <td>0.066703</td>
          <td>0.219910</td>
          <td>1.295623</td>
          <td>0.554399</td>
          <td>11.631811</td>
          <td>14.876004</td>
          <td>0.588699</td>
        </tr>
        <tr>
          <th>15</th>
          <td>0.061973</td>
          <td>0.179125</td>
          <td>4.654101</td>
          <td>0.375086</td>
          <td>11.081292</td>
          <td>10.521093</td>
          <td>0.717606</td>
        </tr>
        <tr>
          <th>16</th>
          <td>0.092996</td>
          <td>0.276388</td>
          <td>3.934188</td>
          <td>0.448308</td>
          <td>13.756615</td>
          <td>12.745539</td>
          <td>0.291697</td>
        </tr>
        <tr>
          <th>17</th>
          <td>0.154490</td>
          <td>0.228798</td>
          <td>3.155262</td>
          <td>0.209700</td>
          <td>14.145472</td>
          <td>13.320453</td>
          <td>0.867700</td>
        </tr>
        <tr>
          <th>18</th>
          <td>0.047826</td>
          <td>0.295456</td>
          <td>1.831015</td>
          <td>0.117507</td>
          <td>13.649584</td>
          <td>11.321448</td>
          <td>0.441672</td>
        </tr>
        <tr>
          <th>19</th>
          <td>0.025519</td>
          <td>0.207379</td>
          <td>3.353283</td>
          <td>0.983222</td>
          <td>11.805350</td>
          <td>11.781888</td>
          <td>0.105139</td>
        </tr>
      </tbody>
    </table>
    </div>



Train-test split
----------------

Next we split the data into a training set (80%) and a testing set
(20%).

.. code:: ipython3

    train_in, test_in, train_out, test_out = train_test_split(
        inputs, outputs, test_size=0.2
    )

Normalization of inputs
-----------------------

To ensure an equal weighting of inputs and outputs in the
backpropagation algorithm fitting the neural network, we have to
normalize the input values. For example window-to-wall ratio is in the
range of 0 to 1 while the :math:`W/`\ m^2$ are in a range of 10 to 15.
Different options for normalization exist. Here we bring all features
(input variables) to have zero mean and a standarddeviation of 1. Note
that we fit the normalizer on training data only.

.. code:: ipython3

    scaler = StandardScaler()
    inputs = scaler.fit_transform(X=train_in)

    scaler_out = StandardScaler()
    outputs = scaler_out.fit_transform(X=train_out)

Hyper-parameters
----------------

Before we start fitting the NN model we define the set of
hyperparameters we want to analyse in our cross-validation to optimize
the model. Here, we select the number of layers of the network as well
as the regularization parameter alpha as parameter value. A larger
number of layers and a lower value of the regularizer lead to higher
variance of the network. This may lead to overfitting. The best
selection may be found using an optimizer like Bayesian Optimization. In
this example we use a simple grid search.

.. code:: ipython3

    hyperparameters = {
        "hidden_layer_sizes": (
            (len(parameters) * 16,),
            (len(parameters) * 16, len(parameters) * 16),
        ),
        "alpha": [1, 10, 10 ** 3],
    }

    neural_net = MLPRegressor(max_iter=1000, early_stopping=False)
    folds = 3

Model fitting
-------------

Here, we use the NN model from ScikitLearn. In a `different
example <../../example-notebooks/How-to-Guides/FitNNTF.html>`__ we use TensorFlow (with and without the Keras
wrapper).

.. code:: ipython3

    clf = GridSearchCV(neural_net, hyperparameters, iid=True, cv=folds)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore", category=FutureWarning)
        clf.fit(inputs, outputs.ravel())


    print(f"Best performing model $R^2$ score on training set: {clf.best_score_}")
    print(f"Model $R^2$ parameters: {clf.best_params_}")
    print(
        f"Best performing model $R^2$ score on a separate test set: {clf.best_estimator_.score(scaler.transform(test_in), scaler_out.transform(test_out))}"
    )


.. parsed-literal::

    Best performing model $R^2$ score on training set: 0.7459884525525834
    Model $R^2$ parameters: {'alpha': 1, 'hidden_layer_sizes': (112, 112)}
    Best performing model $R^2$ score on a separate test set: 0.9416874254967937


Surrogate Modelling Evaluator object
------------------------------------

We can wrap the fitted model in a BESOS ``Evaluator``. This has
identical behaviour to the original EnergyPlus Evaluator object.

.. code:: ipython3

    def evaluation_func(ind, scaler=scaler):
        ind = scaler.transform(X=[ind])
        return (scaler_out.inverse_transform(clf.predict(ind))[0],)


    NN_SM = EvaluatorGeneric(evaluation_func, problem)

This has identical behaviour to the original EnergyPlus Evaluator
object. In the next cells we generate a single input sample and evaluate
it using the surrogate model and EnergyPlus.

.. code:: ipython3

    sample = sampling.dist_sampler(sampling.lhs, problem, 1)
    values = sample.values[0]
    print(values)


.. parsed-literal::

    [ 0.11669079  0.17961374  4.25848042  0.13397506 10.48584932 10.17177171
      0.35134885]


.. code:: ipython3

    NN_SM(values)[0]




.. parsed-literal::

    1814729300.020491



.. code:: ipython3

    evaluator(values)[0]




.. parsed-literal::

    1751712292.4908545



Running a large surrogate evaluation
------------------------------------

.. code:: ipython3

    inputs = sampling.dist_sampler(sampling.lhs, problem, 5000)
    outputs = NN_SM.df_apply(inputs)
    results = inputs.join(outputs)
    results.head()



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=5000.0, style=ProgressStyle(description_w…


.. parsed-literal::






.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Conductivity</th>
          <th>Thickness</th>
          <th>U-Factor</th>
          <th>Solar Heat Gain Coefficient</th>
          <th>ElectricEquipment</th>
          <th>Lights</th>
          <th>Window to Wall Ratio</th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.092928</td>
          <td>0.238438</td>
          <td>4.718053</td>
          <td>0.301978</td>
          <td>14.540408</td>
          <td>13.699712</td>
          <td>0.718998</td>
          <td>2.198494e+09</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.127584</td>
          <td>0.228582</td>
          <td>0.544851</td>
          <td>0.249709</td>
          <td>11.915124</td>
          <td>13.472015</td>
          <td>0.221293</td>
          <td>2.061339e+09</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.039634</td>
          <td>0.158802</td>
          <td>3.416324</td>
          <td>0.086649</td>
          <td>11.756280</td>
          <td>10.131401</td>
          <td>0.758322</td>
          <td>1.823225e+09</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.175974</td>
          <td>0.152255</td>
          <td>4.880733</td>
          <td>0.328792</td>
          <td>12.571576</td>
          <td>10.447726</td>
          <td>0.787714</td>
          <td>1.922154e+09</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.192895</td>
          <td>0.221227</td>
          <td>4.634923</td>
          <td>0.534569</td>
          <td>12.375788</td>
          <td>14.908257</td>
          <td>0.197574</td>
          <td>2.200912e+09</td>
        </tr>
      </tbody>
    </table>
    </div>



Visualization
-------------

.. code:: ipython3

    chart_studio.tools.set_credentials_file(
        username="besos", api_key="Kb2G2bjOh5gmwh1Midwq"
    )
    df = inputs.round(3)

    # generate list if dictionaries
    l = list()
    for i in df.columns:
        l.extend([dict(label=i, values=df[i])])

    l.extend([dict(label=outputs.columns[0], values=outputs.round(-5))])

    data = [
        go.Parcoords(
            line=dict(
                color=outputs["Electricity:Facility"],
                colorscale=[[0, "#D7C16B"], [0.5, "#23D8C3"], [1, "#F3F10F"]],
            ),
            dimensions=l,
        )
    ]

    layout = go.Layout(plot_bgcolor="#E5E5E5", paper_bgcolor="#E5E5E5")

    fig = go.Figure(data=data, layout=layout)
    py.iplot(fig, filename="parcoords-basic")




.. raw:: html


    <iframe
        width="100%"
        height="525px"
        src="https://plotly.com/~besos/1.embed"
        frameborder="0"
        allowfullscreen
    ></iframe>
