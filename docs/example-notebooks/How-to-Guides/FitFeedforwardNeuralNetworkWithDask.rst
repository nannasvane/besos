Fit feedforward Neural Network model With Dask
==============================================

This notebook takes the "Fit feedforward Neural Network model" notebook
and parallelizes the processes using Dask. It will skip over explanation
of code unrelated to Dask. Refer to the "Fit feedforward Neural Network
model" notebook for more details on this notebook.

First import packages, and initialize the scheduler

.. code:: ipython3

    import joblib
    from besos import eppy_funcs as ef, sampling
    from besos.evaluator import EvaluatorEP, EvaluatorGeneric
    from besos.problem import EPProblem
    from dask.distributed import Client
    from sklearn.model_selection import GridSearchCV, train_test_split
    from sklearn.neural_network import MLPRegressor
    from sklearn.preprocessing import StandardScaler
    import warnings
    from parameter_sets import parameter_set

.. code:: ipython3

    from dask.distributed import Client
    client = Client()
    client




.. raw:: html

    <table style="border: 2px solid white;">
    <tr>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Client</h3>
    <ul style="text-align: left; list-style: none; margin: 0; padding: 0;">
      <li><b>Scheduler: </b>tcp://127.0.0.1:36583</li>
      <li><b>Dashboard: </b><a href='/user/peterrwilson99/proxy/8787/status' target='_blank'>/user/peterrwilson99/proxy/8787/status</a></li>
    </ul>
    </td>
    <td style="vertical-align: top; border: 0px solid white">
    <h3 style="text-align: left;">Cluster</h3>
    <ul style="text-align: left; list-style:none; margin: 0; padding: 0;">
      <li><b>Workers: </b>4</li>
      <li><b>Cores: </b>16</li>
      <li><b>Memory: </b>68.72 GB</li>
    </ul>
    </td>
    </tr>
    </table>



We gather the parameters and the building, then create the problem and
evaluator.

.. code:: ipython3

    parameters = parameter_set(7)
    problem = EPProblem(parameters, ["Electricity:Facility"])
    building = ef.get_building()
    problem = EPProblem(parameters, ['Electricity:Facility'])
    evaluator = EvaluatorEP(problem, building)

When df\_apply is called, the dataframe will be processed concurrently.
By passing in the ``processes`` parameter you can define the number of
paritions the dataframe will be divided into. If you are running this
notebook locally, you can open the Dask dashboard. A link is provided by
the ``client`` object (refer to the first cell in the notebook where we
initialized ``Client``). On the dashboard, you can see what processes
are running.

.. code:: ipython3

    %%time
    inputs = sampling.dist_sampler(sampling.lhs, problem, 50)
    outputs = evaluator.df_apply(inputs, processes=4)
    inputs


.. parsed-literal::

    CPU times: user 1.47 s, sys: 287 ms, total: 1.75 s
    Wall time: 23.3 s




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Conductivity</th>
          <th>Thickness</th>
          <th>U-Factor</th>
          <th>Solar Heat Gain Coefficient</th>
          <th>ElectricEquipment</th>
          <th>Lights</th>
          <th>Window to Wall Ratio</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.055023</td>
          <td>0.289414</td>
          <td>0.971157</td>
          <td>0.453890</td>
          <td>11.879346</td>
          <td>12.800983</td>
          <td>0.566172</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.161703</td>
          <td>0.151686</td>
          <td>0.248658</td>
          <td>0.433111</td>
          <td>14.650401</td>
          <td>14.239511</td>
          <td>0.888224</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.146857</td>
          <td>0.293566</td>
          <td>3.995612</td>
          <td>0.905855</td>
          <td>12.620180</td>
          <td>14.457510</td>
          <td>0.120768</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.193907</td>
          <td>0.242702</td>
          <td>0.570291</td>
          <td>0.814085</td>
          <td>12.934642</td>
          <td>13.796069</td>
          <td>0.085228</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.050311</td>
          <td>0.180439</td>
          <td>3.084850</td>
          <td>0.061345</td>
          <td>10.312514</td>
          <td>13.480864</td>
          <td>0.677188</td>
        </tr>
        <tr>
          <th>5</th>
          <td>0.132940</td>
          <td>0.119451</td>
          <td>1.384960</td>
          <td>0.863318</td>
          <td>12.853962</td>
          <td>14.055337</td>
          <td>0.160043</td>
        </tr>
        <tr>
          <th>6</th>
          <td>0.059651</td>
          <td>0.106822</td>
          <td>3.443967</td>
          <td>0.919450</td>
          <td>10.687633</td>
          <td>10.351821</td>
          <td>0.366714</td>
        </tr>
        <tr>
          <th>7</th>
          <td>0.098363</td>
          <td>0.170353</td>
          <td>4.623611</td>
          <td>0.548621</td>
          <td>13.039621</td>
          <td>10.172123</td>
          <td>0.729395</td>
        </tr>
        <tr>
          <th>8</th>
          <td>0.166916</td>
          <td>0.231849</td>
          <td>4.274812</td>
          <td>0.371251</td>
          <td>12.335803</td>
          <td>14.189433</td>
          <td>0.395333</td>
        </tr>
        <tr>
          <th>9</th>
          <td>0.185077</td>
          <td>0.298956</td>
          <td>2.575012</td>
          <td>0.737015</td>
          <td>14.540488</td>
          <td>13.018182</td>
          <td>0.592655</td>
        </tr>
        <tr>
          <th>10</th>
          <td>0.039453</td>
          <td>0.239708</td>
          <td>1.237301</td>
          <td>0.714868</td>
          <td>13.738186</td>
          <td>13.961779</td>
          <td>0.019614</td>
        </tr>
        <tr>
          <th>11</th>
          <td>0.155364</td>
          <td>0.120459</td>
          <td>3.664835</td>
          <td>0.128758</td>
          <td>13.968844</td>
          <td>10.897971</td>
          <td>0.348100</td>
        </tr>
        <tr>
          <th>12</th>
          <td>0.114585</td>
          <td>0.261586</td>
          <td>1.784999</td>
          <td>0.489818</td>
          <td>11.676686</td>
          <td>11.817922</td>
          <td>0.958709</td>
        </tr>
        <tr>
          <th>13</th>
          <td>0.074132</td>
          <td>0.113085</td>
          <td>0.168243</td>
          <td>0.361055</td>
          <td>12.403512</td>
          <td>10.030331</td>
          <td>0.780304</td>
        </tr>
        <tr>
          <th>14</th>
          <td>0.125671</td>
          <td>0.172620</td>
          <td>2.243686</td>
          <td>0.386179</td>
          <td>14.729687</td>
          <td>13.321995</td>
          <td>0.977003</td>
        </tr>
        <tr>
          <th>15</th>
          <td>0.197555</td>
          <td>0.145545</td>
          <td>4.553300</td>
          <td>0.625993</td>
          <td>11.044226</td>
          <td>10.291775</td>
          <td>0.702575</td>
        </tr>
        <tr>
          <th>16</th>
          <td>0.111633</td>
          <td>0.209954</td>
          <td>2.939609</td>
          <td>0.465666</td>
          <td>14.208762</td>
          <td>11.182091</td>
          <td>0.763889</td>
        </tr>
        <tr>
          <th>17</th>
          <td>0.099961</td>
          <td>0.138505</td>
          <td>3.765241</td>
          <td>0.030199</td>
          <td>12.058338</td>
          <td>10.654501</td>
          <td>0.038554</td>
        </tr>
        <tr>
          <th>18</th>
          <td>0.032217</td>
          <td>0.235183</td>
          <td>4.716094</td>
          <td>0.224655</td>
          <td>13.895373</td>
          <td>12.428741</td>
          <td>0.442455</td>
        </tr>
        <tr>
          <th>19</th>
          <td>0.087545</td>
          <td>0.250956</td>
          <td>2.493906</td>
          <td>0.972490</td>
          <td>13.501133</td>
          <td>11.628705</td>
          <td>0.629117</td>
        </tr>
        <tr>
          <th>20</th>
          <td>0.079181</td>
          <td>0.109904</td>
          <td>3.152474</td>
          <td>0.801282</td>
          <td>13.471743</td>
          <td>11.307619</td>
          <td>0.925028</td>
        </tr>
        <tr>
          <th>21</th>
          <td>0.186814</td>
          <td>0.212415</td>
          <td>1.760755</td>
          <td>0.766256</td>
          <td>14.987510</td>
          <td>14.726486</td>
          <td>0.176318</td>
        </tr>
        <tr>
          <th>22</th>
          <td>0.088964</td>
          <td>0.160811</td>
          <td>1.490295</td>
          <td>0.123413</td>
          <td>10.849416</td>
          <td>13.278679</td>
          <td>0.263240</td>
        </tr>
        <tr>
          <th>23</th>
          <td>0.122988</td>
          <td>0.284068</td>
          <td>2.429327</td>
          <td>0.592769</td>
          <td>10.470285</td>
          <td>12.325289</td>
          <td>0.405024</td>
        </tr>
        <tr>
          <th>24</th>
          <td>0.026182</td>
          <td>0.204374</td>
          <td>4.931655</td>
          <td>0.156479</td>
          <td>11.135790</td>
          <td>12.527733</td>
          <td>0.235172</td>
        </tr>
        <tr>
          <th>25</th>
          <td>0.151733</td>
          <td>0.256406</td>
          <td>0.770869</td>
          <td>0.517453</td>
          <td>10.936935</td>
          <td>12.189044</td>
          <td>0.105411</td>
        </tr>
        <tr>
          <th>26</th>
          <td>0.129251</td>
          <td>0.226350</td>
          <td>4.456843</td>
          <td>0.238534</td>
          <td>11.440466</td>
          <td>14.341981</td>
          <td>0.146293</td>
        </tr>
        <tr>
          <th>27</th>
          <td>0.158677</td>
          <td>0.279158</td>
          <td>1.049345</td>
          <td>0.082118</td>
          <td>11.503272</td>
          <td>13.661504</td>
          <td>0.675995</td>
        </tr>
        <tr>
          <th>28</th>
          <td>0.044414</td>
          <td>0.133110</td>
          <td>0.489562</td>
          <td>0.682752</td>
          <td>11.953141</td>
          <td>12.231597</td>
          <td>0.484533</td>
        </tr>
        <tr>
          <th>29</th>
          <td>0.047032</td>
          <td>0.220143</td>
          <td>2.046230</td>
          <td>0.402257</td>
          <td>13.301214</td>
          <td>14.895942</td>
          <td>0.830231</td>
        </tr>
        <tr>
          <th>30</th>
          <td>0.139758</td>
          <td>0.201843</td>
          <td>4.852871</td>
          <td>0.572439</td>
          <td>12.562844</td>
          <td>14.610093</td>
          <td>0.608437</td>
        </tr>
        <tr>
          <th>31</th>
          <td>0.145235</td>
          <td>0.185034</td>
          <td>3.405676</td>
          <td>0.536250</td>
          <td>11.372711</td>
          <td>11.752122</td>
          <td>0.288744</td>
        </tr>
        <tr>
          <th>32</th>
          <td>0.022745</td>
          <td>0.124947</td>
          <td>1.601113</td>
          <td>0.655625</td>
          <td>10.097336</td>
          <td>11.988407</td>
          <td>0.871940</td>
        </tr>
        <tr>
          <th>33</th>
          <td>0.105943</td>
          <td>0.158020</td>
          <td>3.567968</td>
          <td>0.662704</td>
          <td>12.759740</td>
          <td>14.547573</td>
          <td>0.646999</td>
        </tr>
        <tr>
          <th>34</th>
          <td>0.067095</td>
          <td>0.281144</td>
          <td>2.657010</td>
          <td>0.932130</td>
          <td>14.058247</td>
          <td>13.580063</td>
          <td>0.810015</td>
        </tr>
        <tr>
          <th>35</th>
          <td>0.095066</td>
          <td>0.266337</td>
          <td>0.605780</td>
          <td>0.248594</td>
          <td>13.143416</td>
          <td>12.748950</td>
          <td>0.318998</td>
        </tr>
        <tr>
          <th>36</th>
          <td>0.178696</td>
          <td>0.142271</td>
          <td>3.887490</td>
          <td>0.299538</td>
          <td>14.449008</td>
          <td>10.709081</td>
          <td>0.277599</td>
        </tr>
        <tr>
          <th>37</th>
          <td>0.189534</td>
          <td>0.100527</td>
          <td>2.833761</td>
          <td>0.179466</td>
          <td>11.209249</td>
          <td>12.931609</td>
          <td>0.503187</td>
        </tr>
        <tr>
          <th>38</th>
          <td>0.136791</td>
          <td>0.154748</td>
          <td>3.021646</td>
          <td>0.276294</td>
          <td>14.889010</td>
          <td>10.416115</td>
          <td>0.432814</td>
        </tr>
        <tr>
          <th>39</th>
          <td>0.176826</td>
          <td>0.270708</td>
          <td>4.078908</td>
          <td>0.845699</td>
          <td>12.276264</td>
          <td>11.480877</td>
          <td>0.202349</td>
        </tr>
        <tr>
          <th>40</th>
          <td>0.106603</td>
          <td>0.274593</td>
          <td>0.875880</td>
          <td>0.101857</td>
          <td>11.742971</td>
          <td>13.101020</td>
          <td>0.221728</td>
        </tr>
        <tr>
          <th>41</th>
          <td>0.059221</td>
          <td>0.130004</td>
          <td>4.122003</td>
          <td>0.606201</td>
          <td>10.706087</td>
          <td>12.061205</td>
          <td>0.892999</td>
        </tr>
        <tr>
          <th>42</th>
          <td>0.169417</td>
          <td>0.164906</td>
          <td>1.134970</td>
          <td>0.872666</td>
          <td>14.375552</td>
          <td>11.061248</td>
          <td>0.323963</td>
        </tr>
        <tr>
          <th>43</th>
          <td>0.073724</td>
          <td>0.219424</td>
          <td>2.096114</td>
          <td>0.957559</td>
          <td>10.143611</td>
          <td>11.256390</td>
          <td>0.836476</td>
        </tr>
        <tr>
          <th>44</th>
          <td>0.035543</td>
          <td>0.191936</td>
          <td>1.891513</td>
          <td>0.013676</td>
          <td>10.211157</td>
          <td>11.525980</td>
          <td>0.531643</td>
        </tr>
        <tr>
          <th>45</th>
          <td>0.081527</td>
          <td>0.198758</td>
          <td>3.251619</td>
          <td>0.192598</td>
          <td>13.253799</td>
          <td>14.983523</td>
          <td>0.934755</td>
        </tr>
        <tr>
          <th>46</th>
          <td>0.064496</td>
          <td>0.176742</td>
          <td>0.329414</td>
          <td>0.715988</td>
          <td>14.191205</td>
          <td>12.682700</td>
          <td>0.744238</td>
        </tr>
        <tr>
          <th>47</th>
          <td>0.028722</td>
          <td>0.255675</td>
          <td>2.270806</td>
          <td>0.791544</td>
          <td>12.183257</td>
          <td>13.852691</td>
          <td>0.059637</td>
        </tr>
        <tr>
          <th>48</th>
          <td>0.118118</td>
          <td>0.195794</td>
          <td>4.325877</td>
          <td>0.304809</td>
          <td>13.637606</td>
          <td>10.525400</td>
          <td>0.555531</td>
        </tr>
        <tr>
          <th>49</th>
          <td>0.172703</td>
          <td>0.244258</td>
          <td>1.308726</td>
          <td>0.340048</td>
          <td>10.511005</td>
          <td>10.924541</td>
          <td>0.476248</td>
        </tr>
      </tbody>
    </table>
    </div>



Set up model parameters
-----------------------

In this cell, we setup the model. More detail can be found in the "Fit
feedforward Neural Network model" notebook

.. code:: ipython3

    train_in, test_in, train_out, test_out = train_test_split(
        inputs, outputs, test_size=0.2
    )

    scaler = StandardScaler()
    inputs = scaler.fit_transform(X=train_in)

    scaler_out = StandardScaler()
    outputs = scaler_out.fit_transform(X=train_out)

    hyperparameters = {
        "hidden_layer_sizes": (
            (len(parameters) * 16,),
            (len(parameters) * 16, len(parameters) * 16),
        ),
        "alpha": [1, 10, 10 ** 3],
    }

    neural_net = MLPRegressor(max_iter=1000, early_stopping=False)
    folds = 3

Model fitting with Dask
-----------------------

Here, we use the NN model from ScikitLearn. In a `different
example <../../example-notebooks/How-to-Guides/FitNNTF.html>`__ we use TensorFlow (with and without the Keras
wrapper).

Below we parallelize the model fit. Normally, SciketLearn uses joblib to
parallelize model fitting. By specifying the parrallel backend to be
Dask, joblib switches over to using the Dask scheduler. For this
example, using Dask may not be any faster. This is because joblib also
has the ability to parrallelize accross cores. An example where this
tool would be useful is when Dask is using a ditributed network with
access to more cores.

.. code:: ipython3

    %%time
    with joblib.parallel_backend("dask"):
        clf = GridSearchCV(neural_net, hyperparameters, iid=True, cv=folds)
        with warnings.catch_warnings():
            warnings.simplefilter("ignore", category=FutureWarning)
            clf.fit(inputs, outputs.ravel())

    print(f"Best performing model $R^2$ score on training set: {clf.best_score_}")
    print(f"Model $R^2$ parameters: {clf.best_params_}")
    print(
        f"Best performing model $R^2$ score on a separate test set: {clf.best_estimator_.score(scaler.transform(test_in), scaler_out.transform(test_out))}"
    )


.. parsed-literal::

    Best performing model $R^2$ score on training set: 0.9859359222733858
    Model $R^2$ parameters: {'alpha': 1, 'hidden_layer_sizes': (112,)}
    Best performing model $R^2$ score on a separate test set: 0.9973443498852271
    CPU times: user 565 ms, sys: 69.8 ms, total: 635 ms
    Wall time: 4.38 s


Surrogate Modelling Evaluator object
------------------------------------

We can wrap the fitted model in a BESOS ``Evaluator``. This has
identical behaviour to the original EnergyPlus Evaluator object.

The parrallelization occurs when calling the df\_apply function.

.. code:: ipython3

    def evaluation_func(ind, scaler=scaler):
        ind = scaler.transform(X=[ind])
        return (scaler_out.inverse_transform(clf.predict(ind))[0],)


    NN_SM = EvaluatorGeneric(evaluation_func, problem)

Running a large surrogate evaluation
------------------------------------

Here we bump up the sample count to 50,000 and partition the data into
4. (if you have more cores available, feel free to try increasing the
proccesses)

.. code:: ipython3

    %%time
    inputs = sampling.dist_sampler(sampling.lhs, problem, 50000)
    outputs = NN_SM.df_apply(inputs, processes=4)
    results = inputs.join(outputs)
    results.head()


.. parsed-literal::

    CPU times: user 724 ms, sys: 149 ms, total: 873 ms
    Wall time: 9.38 s




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>Conductivity</th>
          <th>Thickness</th>
          <th>U-Factor</th>
          <th>Solar Heat Gain Coefficient</th>
          <th>ElectricEquipment</th>
          <th>Lights</th>
          <th>Window to Wall Ratio</th>
          <th>Electricity:Facility</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>0</th>
          <td>0.192527</td>
          <td>0.153194</td>
          <td>4.481314</td>
          <td>0.567995</td>
          <td>11.835818</td>
          <td>13.052947</td>
          <td>0.857530</td>
          <td>2.057846e+09</td>
        </tr>
        <tr>
          <th>1</th>
          <td>0.080337</td>
          <td>0.110154</td>
          <td>2.589947</td>
          <td>0.811769</td>
          <td>11.163278</td>
          <td>13.104261</td>
          <td>0.966630</td>
          <td>1.995323e+09</td>
        </tr>
        <tr>
          <th>2</th>
          <td>0.095729</td>
          <td>0.285002</td>
          <td>4.567989</td>
          <td>0.364190</td>
          <td>14.112162</td>
          <td>12.881720</td>
          <td>0.017776</td>
          <td>2.154193e+09</td>
        </tr>
        <tr>
          <th>3</th>
          <td>0.156267</td>
          <td>0.214472</td>
          <td>2.800968</td>
          <td>0.401643</td>
          <td>11.264160</td>
          <td>11.086406</td>
          <td>0.247729</td>
          <td>1.882376e+09</td>
        </tr>
        <tr>
          <th>4</th>
          <td>0.196672</td>
          <td>0.273954</td>
          <td>2.064289</td>
          <td>0.267733</td>
          <td>10.874480</td>
          <td>10.699428</td>
          <td>0.980151</td>
          <td>1.845306e+09</td>
        </tr>
      </tbody>
    </table>
    </div>
