Morris Screening
================

In this notebook we apply the popular Morris screening method to a
building design problem. We determine the sensitivty of the objective
(electricty use) to each of the design parameters.

.. code:: ipython3

    import numpy as np
    import pandas as pd
    from besos import eppy_funcs as ef, sampling
    from besos.evaluator import EvaluatorEP
    from besos.problem import EPProblem

    from SALib.analyze import morris as manalysis
    from SALib.sample import morris as msampling
    from parameter_sets import parameter_set

Build an EnergyPlus Evaluator
-----------------------------

.. code:: ipython3

    parameters = parameter_set(7)  # use a pre-defined parameter set
    problem = EPProblem(parameters, ["Electricity:Facility"])
    building = ef.get_building()  # use the example building
    evaluator = EvaluatorEP(problem, building)
    inputs = sampling.dist_sampler(
        sampling.lhs, problem, 50
    )  # get 50 samples of the input space

Conduct a Morris screening of the parameters
--------------------------------------------

The following cells conduct a Morris screening, a global sensitivity
method. It uses :math:`r` times :math:`n` one-at-time changes (OAT) of
each parameter at randomly selected points. The resulting distribution
of :math:`r` samples provides a mean :math:`\mu^*`, and a standard
deviation :math:`\sigma` of the elementary effects of the :math:`i`-th
input parameter.
[`1 <https://www.sciencedirect.com/science/article/pii/S1364032112007101>`__]
[`2 <https://en.wikipedia.org/wiki/Elementary_effects_method>`__]
[`3 <https://en.wikipedia.org/wiki/Morris_method>`__]

.. code:: ipython3

    names = [parameters[i].name for i in range(len(parameters))]
    bounds = [
        [parameters[i].value_descriptor.min, parameters[i].value_descriptor.max]
        for i in range(len(parameters))
    ]

    problem = {"num_vars": len(parameters), "names": names, "bounds": bounds}

    X = np.round(msampling.sample(problem, N=5), decimals=3)
    inputs = pd.DataFrame(data=X, columns=names)
    outputs = evaluator.df_apply(inputs)


    Y = outputs.values
    Si = manalysis.analyze(
        problem, X, Y, conf_level=0.95, print_to_console=True, num_levels=4
    )
    pd.DataFrame(data=Si["mu_star"], index=Si["names"]).sort_values(by=0)


.. parsed-literal::

    /usr/local/lib/python3.7/dist-packages/ipykernel_launcher.py:1: DeprecationWarning: Call to deprecated function (or staticmethod) name. (Parameters are no longer nameable. Use the name(s) of this Parameter's Descriptor(s) instead)
      """Entry point for launching an IPython kernel.
    /usr/local/lib/python3.7/dist-packages/ipykernel_launcher.py:4: DeprecationWarning: Call to deprecated function (or staticmethod) value_descriptor. (Does not support multiple Descriptors per Parameter.Use the value_descriptors of this Parameter instead.)
      after removing the cwd from sys.path.



.. parsed-literal::

    HBox(children=(FloatProgress(value=0.0, description='Executing', max=40.0, style=ProgressStyle(description_wid…


.. parsed-literal::


    Parameter                         Mu_Star         Mu    Mu_Star_Conf      Sigma
    Conductivity                   55103409.346 55103409.346    17985359.435 23420822.132
    Thickness                      8762429.111 -3179210.608     4804181.550 10952322.351
    U-Factor                       6110674.908 -331459.167     2859462.946 7850274.354
    Solar Heat Gain Coefficient    53260108.201 53260108.201     2573641.747 3537803.028
    ElectricEquipment              319724112.495 319724112.495     1720440.632 2261602.448
    Lights                         308868949.080 308868949.080     4702142.104 6145866.519
    Window to Wall Ratio                0.000      0.000           0.000      0.000




.. raw:: html

    <div>
    <style scoped>
        .dataframe tbody tr th:only-of-type {
            vertical-align: middle;
        }

        .dataframe tbody tr th {
            vertical-align: top;
        }

        .dataframe thead th {
            text-align: right;
        }
    </style>
    <table border="1" class="dataframe">
      <thead>
        <tr style="text-align: right;">
          <th></th>
          <th>0</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>Window to Wall Ratio</th>
          <td>0.000000e+00</td>
        </tr>
        <tr>
          <th>U-Factor</th>
          <td>6.110675e+06</td>
        </tr>
        <tr>
          <th>Thickness</th>
          <td>8.762429e+06</td>
        </tr>
        <tr>
          <th>Solar Heat Gain Coefficient</th>
          <td>5.326011e+07</td>
        </tr>
        <tr>
          <th>Conductivity</th>
          <td>5.510341e+07</td>
        </tr>
        <tr>
          <th>Lights</th>
          <td>3.088689e+08</td>
        </tr>
        <tr>
          <th>ElectricEquipment</th>
          <td>3.197241e+08</td>
        </tr>
      </tbody>
    </table>
    </div>
