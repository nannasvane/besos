============================
Customizing EnergyHub Solver
============================

Currently we support calling `GLPK <https://www.gnu.org/software/glpk/>`_, `CPlex <https://www.ibm.com/analytics/cplex-optimizer>`_ and `Gurobi <https://www.gurobi.com/>`_ solvers. GLPK is freely available through our platform however if you have a licence to use CPlex or Gurobi you can configre those using the following methods.

You can set the linear solver for the EnergyHub by setting the ``solver_settings``. GLPK is the default solver therefore, if you can call ``glpsol`` from the terminal you don't need to setup anything and it will call the GLPK solver.

For example:

.. code-block:: python3

    from pyehub.energy_hub.ehub_model import EHubModel
    from pyehub.energy_hub.utils import constraint
    from pyehub.outputter import pretty_print
    excel_file = './examples/EnergyHub/test_file.xlsx' # name of the excel file.
    my_model = EHubModel(excel=excel_file) # instantiate our model. Nothing is solved at this point.
    results = my_model.solve(solver_settings=solver_settings) # set your solver_settings
    pretty_print(results) # print the results to the console

You can configure GLPK with different options or a different path using the following example.

.. code-block:: python3

    solver_settings={
                    'name': 'glpk',
                    'solver_path':'glpsol',
                    'options': ['--mipgap', '0.05'],
                    }


To set Gurobi use:

.. code-block:: python3

    solver_settings={
                    'name': 'gurobi',
                    'options':None,
                    }



To set CPlex use:

.. code-block:: python3

    solver_settings={'name': 'cplex',}


.. tip::
    ``solver_path`` and ``options`` will work for cplex and gurobi based on the command line documentation which can be found `here <https://www.gurobi.com/documentation/9.0/refman/grb_command_line_tool.html>`_ for Gurobi and `here <https://www.ibm.com/support/knowledgecenter/SSSA5P_12.10.0/ilog.odms.ide.help/OPL_Studio/refoplrun/topics/oplrun_syntax_commandline.html>`_ for CPlex.
