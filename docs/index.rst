*********************************
BESOS documentation
*********************************
-----------------------------------------------------------------------
besos: Building and Energy Systems Optimization and Surrogate-modelling
-----------------------------------------------------------------------

besos library and BESOS platform have been created by the Energy In Cities Group and University of Victoria. The Platform is a Jupyter Hub that is able to run the besos code base, and has all the dependencies installed. The platform is freely accessible for academics in the building energy modelling space.

.. image:: images/besos.png

Besos library connects modelling and optimization tools to allow for parameterizing
running and optimizing models, sampling from their design spaces, and generating data for use
in machine learning models. This supports designing building and district energy modelling experiments.

.. image:: https://gitlab.com/energyincities/besos/badges/master/pipeline.svg
   :target: https://gitlab.com/energyincities/besos/-/commits/master
.. image:: https://gitlab.com/energyincities/besos/badges/master/coverage.svg
   :target: https://gitlab.com/energyincities/besos/
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
.. image:: https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white
   :target: https://github.com/pre-commit/pre-commit



Examples of applications of Besos to building and district energy modelling are available on our `examples overview page <example-notebooks/Overview/ExamplesOverview.html>`_ or take a look at the `example workflow <building_to_surrogate_tf.html>`_.

`EnergyPlus <https://energyplus.net/>`_ is a commonly used physics-based building energy modelling software. `EnergyHub <https://python-ehub.readthedocs.io>`_ is a linear programming framework with constraints specificially designed to size energy convertors and schedule energy carriers for an energy system with the intend to minimize cost, emissions or a custom objective funcation. Both these simulation tools are commenly used by civil engineering researchers to build energy related simulation experiments. besos adds substantial functionality to these tools integrating them with python and several different libraries to optimize the model, or use the results from the models for and machine learning.

`besos` also facilitates running large-scale parametric analyses of `EnergyPlus <https://energyplus.net/>`_ or `EnergyHub <https://python-ehub.readthedocs.io>`_ models with output in a pandas DataFrame and uses this to train machine learning surrogate models with scikit-learn or TensorFlow. We provide access to commonly used optimization algorithms via existing optimization toolboxes.


`besos` library and associated BESOS Platform
=============================================
The `BESOS Platform <https://besos.uvic.ca/>`_ is a Jupyter Hub that is able to run the besos code base, and has all the dependencies installed. An overview of the platform using a C4 diagram is presented :doc:`here <under_the_hood>`. The platform is freely accesible for academics in the building energy modelling space. If you want to get started on the BESOS platform, check out how to get started `here! <https://gitlab.com/energyincities/besos/-/blob/master/docs/Getting%20Started%20on%20BESOS%20and%20making%20%20your%20first%20Notebook.pdf>`_

The `besos library <https://gitlab.com/energyincities/besos>`_ connects modelling and optimization tools to allow for parameterizing running and optimizing models, sampling from their design spaces, and generating data for use in machine learning models. This supports designing building and district energy modelling experiments.

Installation
==================
If you want to run besos on your own personal computer, please review the `Installation page <installation.html>`_ on the gitlab. If you are an scholar associated with an academic institution you can request access to the BESOS platform through our `online request form <https://docs.google.com/forms/d/e/1FAIpQLSfqDFE0Zd0L7VREVJyF4XXvR2iWIaUxjPFuIyY6qXK-wvIoww/viewform>`_. This platform comes with all the software dependies installed and has significant computational resources avilable.


A high level overview of the besos modules.
===========================================

The besos library software is written to include all the features required to setup a building or district design energy modelling experiments using EnergyPlus as building energy model and EnergyHub as distric energy model. We have created distinct software objects that will help the designer create this experiment.


The following concepts: **Parameters, Objectives, Problem, Evaluators, Sampling and Optimizers** have been created to build these experiments. First is the creation of the optimization problem which is composed of a model, parameters and an objective. The model could be an `EnergyPlus <https://energyplus.net>`_ model or a `PyEHub <https://python-ehub.readthedocs.io>`_ model. Parameters are created is a separate class where each parameter is composed of a selector and a descriptor. The  `selectors <example-notebooks/Reference/Selectors.html>`_ point to the definitions in the model that will be changed and the `descriptors <example-notebooks/Reference/Descriptors.html>`_ provide the range or list of values that are of interest. The objectives point to model results that are of interest to the optimizer. Once a problem has been defined we can make use of the `Evaluators <example-notebooks/Reference/Evaluators.html>`_ to run the model to find a sampled dataset or allow a optimizer to find the best solution given the problem definition. The sampling dataset can be used to train a surrogate model. To get a better understanding how all the concepts work together, we have constructed an :doc:`example-notebooks/building_to_surrogate_tf` that explains these basic concepts.


Examples
==================
The example workflow only gives a high level overview of what is possible. We have prepared more detailed examples of all the various modules and case studies of the besos library. The examples can be found on our `examples overview page <example-notebooks/Overview/ExamplesOverview.html>`_

.. toctree::
	:maxdepth: 1
	:caption: besos library
	:glob:
	:hidden:

	installation
	file_blurbs.rst
	setting_a_custom_solver.rst
	contribute_to_the_code
	faq_l


.. toctree::
	:maxdepth: 1
	:caption: BESOS Platform
	:glob:
	:hidden:

  building_to_surrogate_tf
	under_the_hood
	formatting
	faq_p


.. toctree::
	:maxdepth: 2
	:caption: Overview
	:glob:
	:hidden:

	example-notebooks/Overview/*

.. toctree::
	:maxdepth: 2
	:caption: How-to Guides
	:glob:
	:hidden:

	example-notebooks/How-to-Guides/*

.. toctree::
	:maxdepth: 2
	:caption: Tutorials
	:glob:
	:hidden:

	example-notebooks/Tutorials/*

.. toctree::
	:maxdepth: 2
	:caption: Reference
	:glob:
	:hidden:

	example-notebooks/Reference/*




.. toctree::
	:maxdepth: 2
	:caption: Modules
	:glob:
	:hidden:

	modules/*

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


Acknowledgement
==================
besos library and BESOS platform have been created by the Energy In Cities Group and University of Victoria.
