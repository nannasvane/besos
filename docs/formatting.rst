============================
Formatting Jupyter Notebooks
============================

This guide outlines some recommendations for layout and formatting of notebooks that are submitted as Examples on BESOS.

File Naming
=============
Use CamelCaseLikeThis as your file name, so it can be linked to easily.

Table of Contents
=================
Use the Table of Contents extension: `toc2 extension <https://jupyter-contrib-nbextensions.readthedocs.io/en/latest/nbextensions/toc2/README.html>`_

Abstract
=============
As for a paper, the abstract should give a summary of the notebook, including the objectives of the analysis, input data needed and what results it generates.

Introduction
=============
One to two paragraphs explaining the relevant background of the analysis and the problem is is trying to solve. It should also include references to any relevant existing notebooks, academic literature and data sources.

Imports
=============
Put all required modules in an `import cell`, rather than scattered through the notebook.
Start with generic things like pandas, then specific external modules like platypus, then the local besos modules.
Non-standard modules should include a comment explaining what the module is for and including a link to the repository or documentation.

For each analysis section:
==========================
1.  Begin each section with a `markdown cell` with a section title and description
2.  The `code cell` should include comments describing any non-trivial lines
3.  The code will return a `results cell`, likely a plot or numerical result
4.  Optional: Include a `comments cell` following the plots to describe what the results are showing.
