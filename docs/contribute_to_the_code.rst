*********************************
Contribute to the code
*********************************


Code contributions
==================

If you are making a new feature, first install besos locally, and make a feature branch off of dev. (If you are fixing a bug branch off of the master branch.) Once you have written your code, test and format it, then submit a merge request. These steps are described in more detail below.

Development Installation
========================
You will need python 3 and git installed in order to install besos.

Download the repo:

.. code-block:: python3

    git clone https://gitlab.com/energyincities/besos.git
    cd besos
    git checkout dev

Set up a virtual environment (optional but recommended):

.. code-block:: python3

    python3 -m venv venv
    source venv/bin/activate

Install besos in editable mode, with development dependencies:

.. code-block:: python3

    pip install -e .
    pip install -r environments/requirements-dev.txt
    pre-commit install


Feature branches
================
To create an feature branch from dev, use

.. code-block:: python3

    git checkout dev
    git pull
    git checkout -b <your-branch-name>


Testing and Formatting
======================

You can run the tests using `pytest`, and format code using `black .` (both from the besos root directory.) Tests and formatting will also be run automatically by gitlab-ci on any merge request. Every month the master branch also runs all example notebooks.

Design Notes
============

The primary purpose of these tools is to facilitate combining building simulation tools, machine learning techniques, and optimisation algorithms. It does not attempt to provide new tools in any of these domains.

Two dimensional data should be stored in or converted to a DataFrame where possible, especially for user facing data.

Reasonable defaults should be available where possible.

There should be simple versions of core features available which can be used out of the box.


Provenance
===========

BESOS releases will be authorized by Paul Kovacs (pkovacs@uvic.ca) along with the help of Ralph Evins (revins@uvic.ca) and Theo Christiaanse (theochri@uvic.ca) who will validate the scientific content.

New additions and features to the code base must pass all current tests in the CI pipeline.  In addition to passing current tests, extended functionality must come packaged with tests to validate the extension for future releases.  If you have a contribution you would like to make, submit a merge request along with a description of the extended functionality and its intended use.  From there the BESOS team will evaluate and validate the code for future releases.

With each major release iteration (x.x.0 or higher), additional functions and classes will be documented in our readthedocs pages and a general overview of the changes can be found in the changelog/release notes.

The BESOS platform intends to keep up with upgrades and patches of the third-party software implemented within the platform.  Upgrades and patches will be implemented on a necessity basis (i.e. known issues and/or features requests could be implemented before non-critical third-party upgrades/patches are applied).
